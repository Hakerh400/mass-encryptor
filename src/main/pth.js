'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../omikron');

const pth_join = (pth1, pth2) => {
  const pth = pth1 === '' ? pth2 : `${pth1}/${pth2}`;
  return pth.replace(/[\/\\]+/g, '/');
};

module.exports = {
  join: pth_join,
};