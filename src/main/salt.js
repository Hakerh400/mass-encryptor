'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../omikron');

const salt_size = 8;

const gen = (size=salt_size) => {
  return gen_rand_bytes(size);
};

const gen_rand_bytes = size => {
  const buf = Buffer.alloc(size);
  
  for(let i = 0; i !== size; i++)
    buf[i] = rand(256);

  return buf;
};

const rand = (...args) => {
  const n = Date.now();
  while(Date.now() === n) Math.random();
  return O.rand(...args);
};

module.exports = {
  size: salt_size,
  gen_rand_bytes,
  gen,
};