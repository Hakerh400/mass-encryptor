'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const crypto = require('crypto');
const O = require('../omikron');
const Salt = require('./salt');

const {min, max} = Math;

const hash_size = 64;
const hash_zero = Buffer.alloc(hash_size);

const buf8 = Buffer.alloc(8);
const bufh = Buffer.alloc(hash_size);

class FSE {
  constructor(pw, salt=null){
    if(salt === null)
      salt = Salt.gen();
    
    this.salt = salt;
    this.hash = calc_hash2(pw, salt);
    this.enc_mode = 1;
  }

  set_enc_mode(enc_mode){
    this.enc_mode = enc_mode ? 1 : 0;
  }

  open(pth, pw, mode){
    const hash = calc_hash2(this.hash, pw);
    const file = new FSE_File(hash, pth, mode);

    file.set_enc_mode(this.enc_mode);

    return file;
  }
}

class FSE_File {
  constructor(hash, pth, mode){
    const fd = fs.openSync(pth, mode);

    this.fd = fd;
    this.hash0 = hash;
    this.addr = 0;
    this.addr1 = 0;

    this.hash_i = null;
    this.hash = null;
    this.enc_mode = 0;
    this.keep_buf = 0;
  }
  
  close(){
    fs.closeSync(this.fd);
  }

  set_enc_mode(enc_mode){
    if(enc_mode){
      this.enc_mode = 1;
      return;
    }

    this.enc_mode = 0;
    this.i = null;
    this.hash = hash_zero;
  }

  get_hash0(){
    return this.hash0;
  }

  get_hash(i){
    if(!this.enc_mode) return this.hash;

    if(i === this.hash_i)
      return this.hash;

    this.hash_i = i;

    const hash = calc_hash2(this.hash0, String(i));
    this.hash = hash;

    return hash;
  }

  get_addr(){
    return this.addr;
  }

  write(buf, offset, len, i=null){
    const {fd} = this;

    if(!this.keep_buf) buf = Buffer.from(buf);

    if(offset === null) offset = 0;
    if(len === null) len = buf.length;

    if(i === null){
      i = this.addr;
      this.addr = i + len;
    }
    
    let offset0 = offset;
    let len0 = len;
    let i0 = i;

    // let _len0 = len;
    // let _len = 0;

    let hash_i = i / hash_size | 0;

    while(len !== 0){
      const hash = this.get_hash(hash_i);
      const i0 = hash_i * hash_size;
      const j0 = i - i0;
      const len1 = min(len, hash_size - j0);

      // _len += fs.writeSync(fd, buf, offset, len1, i);

      for(let j = 0; j !== len1; j++)
        buf[offset + j] ^= hash[j0 + j];
      
      len -= len1;
      offset += len1;
      i += len1;
      hash_i++;
    }
    
    // assert(_len0 === _len);

    fs.writeSync(fd, buf, offset0, len0, i0);

    return len;
  }

  write_buf(buf, i){
    this.write(buf, 0, null, i);
  }

  write_uint(n, i){
    buf8.writeBigUInt64LE(BigInt(n));
    this.write_buf(buf8, i);
  }

  write_addr(j){
    let i = this.addr;
    this.write_uint(0, j);
    return i;
  }

  write_str(str, i=null){
    const buf = Buffer.from(str);
    this.write_uint(buf.length, i);

    if(i !== null) i += 8;
    this.write_buf(buf, i);
  }

  read(buf, offset, len, i=null){
    const {fd} = this;

    if(offset === null) offset = 0;
    if(len === null) len = buf.length;

    if(i === null){
      i = this.addr;
      this.addr = i + len;
    }

    fs.readSync(fd, buf, offset, len, i);

    // let _len0 = len;
    // let _len = 0;

    let hash_i = i / hash_size | 0;

    while(len !== 0){
      const hash = this.get_hash(hash_i);
      const i0 = hash_i * hash_size;
      const j0 = i - i0;
      const len1 = min(len, hash_size - j0);

      // _len += fs.writeSync(fd, buf, offset, len1, i);

      for(let j = 0; j !== len1; j++)
        buf[offset + j] ^= hash[j0 + j];

      len -= len1;
      offset += len1;
      i += len1;
      hash_i++;
    }

    // assert(_len0 === _len);

    return len;
  }

  read_buf(buf, i){
    if(typeof buf === 'number'){
      const n = buf;
      buf = Buffer.alloc(n);
    }

    this.read(buf, 0, null, i);
    return buf;
  }

  read_uint(i){
    this.read_buf(buf8, i);
    return Number(buf8.readBigUInt64LE());
  }

  read_str(i=null){
    const len = this.read_uint(i);

    if(i !== null) i += 8;
    const buf = this.read_buf(len, i);

    this.addr1 = i !== null ? i + len : this.addr;

    return buf.toString();
  }
}

const calc_hash = a => {
  const hash = crypto.createHash('sha512');
  hash.update(to_buf(a));
  return hash.digest();
};

const calc_hash2 = (a, b) => {
  return calc_hash(Buffer.concat([to_buf(a), to_buf(b)]));
};

const to_buf = a => {
  if(Buffer.isBuffer(a)) return a;
  return Buffer.from(a);
};

module.exports = Object.assign(FSE, {
  FSE_File,
  calc_hash,
  calc_hash2,
});