'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../omikron');

class BufBuilder {
  buf = Buffer.alloc(1);
  buf_len = 1;
  size = 0;

  get_buf(copy){
    return slice(this.buf, 0, this.size, copy);
  }

  expand(){
    const {buf, buf_len} = this;
    const bufs = [buf, Buffer.alloc(buf_len)];

    this.buf = Buffer.concat(bufs);
    this.buf_len = buf_len * 2;
  }

  inc_size(n){
    const size = this.size + n;
    
    while(this.buf_len < size)
      this.expand();
  
    this.size = size;
  }

  write_byte(b){
    const i = this.size;
    this.inc_size(1);
    this.buf[i] = b;
  }

  write_uint64(n){
    const i = this.size;
    this.inc_size(8);
    this.buf.writeBigUint64LE(BigInt(n), i);
  }
}

const slice = (buf, start, end, copy) => {
  const buf1 = buf.subarray(start, end);
  if(!copy) return buf1;
  return Buffer.from(buf1);
};

module.exports = BufBuilder;