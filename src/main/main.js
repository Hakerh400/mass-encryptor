'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../omikron');
const config = require('../config');
const format = require('../../../node-projects/format');
const Salt = require('./salt');
const Path = require('./pth');
const FSE = require('./fse');

const {min, max} = Math;
const {size: salt_size} = Salt;
const {join: pth_join} = Path;

const MODE = 4

const flags = {
  show_pw: 1,
  rm_dir: 1,
};

const tick_n = 1e3;
const index_file_name = '\x5F';

const chunk_size = 1 << 30;
const chunk_buf = Buffer.alloc(chunk_size);

const cwd = __dirname;
const style_file = pth_join(cwd, 'style.css');

const div = O.ce(O.body, 'div');

const test_dir = '/home/user/Git/test-enc';
const src_dir = pth_join(test_dir, 'inp');
const dest_dir = pth_join(test_dir, 'out');
const dest_dir2 = pth_join(test_dir, 're');

const test_pw = 'P4sSw0rd';

let tick_i = 0;

const main = () => {
  add_style();

  // const fse = new FSE(test_pw);
  // const test_file = pth_join(test_dir, 'test.bin');
  // const fd = fse.open(test_file, 'w');

  // log(fse.fds);
  // fse.close(fd);
  // log(fse.fds);

  // return;

  open_main_menu();
};

const init_div = () => {
  div.innerText = '';
};

const open_main_menu = () => {
  init_div();

  const btn_enc_dir = ce_btn('Encrypt folder', open_enc_dir_menu);
  const btn_dec_dir = ce_btn('Decrypt folder', open_dec_dir_menu);
  const btn_dec_file = ce_btn('Decrypt file', open_dec_file_menu);
  const btn_browse = ce_btn('Browse', open_browse_menu);

  if(MODE === 1) btn_enc_dir.click();
  else if(MODE === 2) btn_dec_dir.click();
  else if(MODE === 3) btn_dec_file.click();
  else if(MODE === 4) btn_browse.click();
};

const open_enc_dir_menu = () => {
  init_div();

  const src = ce_inp('Source');
  const dest = ce_inp('Destination');
  const pw = ce_inp('Password', 1);

  const btn = ce_btn('Encrypt folder', () => {
    return encrypt_dir(src.value, dest.value, pw.value);
  });

  if(MODE){
    src.value = src_dir;
    dest.value = dest_dir;
    pw.value = test_pw;

    btn.click();
  }
};

const open_dec_dir_menu = () => {
  init_div();

  const src = ce_inp('Source');
  const dest = ce_inp('Destination');
  const pw = ce_inp('Password', 1);

  const btn = ce_btn('Decrypt folder', () => {
    return decrypt_dir(src.value, dest.value, pw.value);
  });

  if(MODE){
    src.value = dest_dir;
    dest.value = dest_dir2;
    pw.value = test_pw;

    btn.click();
  }
};

const open_dec_file_menu = () => {
  init_div();

  const src = ce_inp('Source');
  const dest = ce_inp('Destination');
  const pw = ce_inp('Password', 1);

  const btn = ce_btn('Decrypt file', () => {
    return decrypt_file(src.value, dest.value, pw.value);
  });

  if(MODE){
    src.value = '/home/user/Git/test-enc/out/theory/core/axioms.pure';
    dest.value = '/home/user/Git/test-enc/1.txt';
    pw.value = test_pw;

    btn.click();
  }
};

const open_browse_menu = () => {
  init_div();

  const src = ce_inp('Source');
  const pw = ce_inp('Password', 1);

  const btn = ce_btn('Browse', () => {
    return browse(src.value, pw.value);
  });

  if(MODE){
    src.value = dest_dir;
    pw.value = test_pw;

    btn.click();
  }
};

const encrypt_dir = async (src_pth, dest_pth, pw) => {
  if(flags.rm_dir) cp.execSync(`rm -rf ${dest_pth}`);

  if(!fs.existsSync(dest_pth))
    fs.mkdirSync(dest_pth);

  const fse = new FSE(pw);

  let n_dirs = 0;
  let n_files = 0;
  let total_size = 0;

  analyze: {
    const lab_status = ce_lab('Analyzing content...');
    const lab_info = ce_lab();

    const update_status = async () => {
      lab_info.innerText = [
        ['Folders', n_dirs],
        ['Files', n_files],
        ['Total size', total_size, 1],
      ].map(([a, b, c=0]) => {
        let s = format.num(b);

        if(c){
          s = `${s} B`;
          const s1 = format.bytes(b);
          if(s1 !== s) s = `${s1} (${s})`;
        }

        return `${a}: ${s}`;
      }).join('\n');
      await tick();
    };

    await update_status();

    const dirs = [src_pth];

    while(dirs.length !== 0){
      const dir = dirs.pop();
      const names = fs.readdirSync(dir);

      for(const name of names){
        const pth = pth_join(dir, name);

        const stat = get_stat(pth);
        if(stat === null) continue;

        if(stat.isDirectory()){
          n_dirs++;
          dirs.push(pth);
          continue;
        }

        if(stat.isFile()){
          n_files++;
          total_size += stat.size;
          continue;
        }

        assert.fail(pth);
      }

      await update_status();
    }

    lab_status.innerText += ' Done';
  }

  let cur_size = 0;

  const lab_status = ce_lab();
  const pb = O.ce(div, 'progress');
  pb.max = max(total_size, 1);

  let s_prev = null;

  const update_status = async () => {
    let s = percent(total_size, cur_size);
    if(s === s_prev) return;

    s_prev = s;
    lab_status.innerText = `Encrypting... ${s}`;
    pb.value = total_size === 0 ? 1 : cur_size;

    return tick(1);
  };

  await update_status();

  const index_file_pth = index_file_name;
  const index_file_pth_act = pth_join(dest_pth, index_file_pth);
  const index_file = fse.open(index_file_pth_act,
    index_file_pth, 'w');
  
  index_file.set_enc_mode(0);
  index_file.write_buf(fse.salt);
  index_file.set_enc_mode(1);

  index_file.write_uint(0);
  index_file.write_uint(n_dirs);
  index_file.write_uint(n_files);
  index_file.write_uint(total_size);
  
  const queue = [[src_pth, '', null]];

  while(queue.length !== 0){
    const [dir, dir1, addr] = queue.shift();
    const names = fs.readdirSync(dir);

    if(addr !== null){
      const addr1 = index_file.get_addr();
      index_file.write_uint(addr1, addr);
    }

    const files = [];
    const dirs = [];

    for(const name of names){
      const pth = pth_join(dir, name);
      const stat = get_stat(pth);
      if(stat === null) continue;

      if(stat.isFile()){
        files.push(name);
        continue;
      }

      if(stat.isDirectory()){
        dirs.push(name);
        continue;
      }

      // assert.fail(stat);
      continue;
    }

    const files_num = files.length;
    const dirs_num = dirs.length;

    index_file.write_uint(files_num);
    const file_addrs = O.ca(files_num, () => index_file.write_addr());
    
    index_file.write_uint(dirs_num);
    const dir_addrs = O.ca(dirs_num, () => index_file.write_addr());

    for(let i = 0; i !== files_num; i++){
      const name = files[i];
      const pth = pth_join(dir, name);
      const pth1 = pth_join(dir1, `\x5F${i}`);
      const pth1_act = pth_join(dest_pth, pth1);

      const addr = index_file.get_addr();
      index_file.write_uint(addr, file_addrs[i]);
      index_file.write_str(name);
      
      // Encrypt file
      
      const stat = get_stat(pth);
      const {size} = stat;
      const fd = fs.openSync(pth, 'r');
      const file = fse.open(pth1_act, pth1, 'w');

      file.keep_buf = 1;

      for(let i = 0; i !== size;){
        const len = min(chunk_size, size - i);
        
        fs.readSync(fd, chunk_buf, 0, len);
        file.write(chunk_buf, 0, len);

        i += len;
      }

      fs.closeSync(fd);
      file.close();
      
      cur_size += size;
      await update_status();
    }

    for(let i = 0; i !== dirs_num; i++){
      const name = dirs[i];
      const pth = pth_join(dir, name);
      const pth1 = pth_join(dir1, `${i}`);
      const pth1_act = pth_join(dest_pth, pth1);
      
      const addr = index_file.get_addr();
      index_file.write_uint(addr, dir_addrs[i]);
      index_file.write_str(name);
      
      fs.mkdirSync(pth1_act);
      
      const addr1 = index_file.write_addr();
      queue.push([pth, pth1, addr1]);
    }
  }

  index_file.close();

  assert(cur_size === total_size);

  ce_lab().innerText = 'Done';
};

const decrypt_dir = async (src_pth, dest_pth, pw) => {
  if(flags.rm_dir) cp.execSync(`rm -rf ${dest_pth}`);

  if(!fs.existsSync(dest_pth))
    fs.mkdirSync(dest_pth);

  const index_file_pth = index_file_name;
  const index_file_pth_act = pth_join(src_pth, index_file_pth);

  const fd = fs.openSync(index_file_pth_act, 'r');
  const salt = Buffer.alloc(salt_size);
  const salt_size1 = fs.readSync(fd, salt, 0, salt_size, 0);

  fs.closeSync(fd);
  assert(salt_size1 === salt_size);

  const fse = new FSE(pw, salt);

  const index_file = fse.open(index_file_pth_act,
    index_file_pth, 'r');
  
  const csum = index_file.read_uint(salt_size);

  if(csum !== 0){
    O.body.innerText = 'Wrong password';
    return;
  }

  const n_dirs = index_file.read_uint(salt_size + 8);
  const n_files = index_file.read_uint(salt_size + 8 * 2);
  const total_size = index_file.read_uint(salt_size + 8 * 3);

  show_stats: {
    const lab_info = ce_lab();

    lab_info.innerText = [
      ['Folders', n_dirs],
      ['Files', n_files],
      ['Total size', total_size, 1],
    ].map(([a, b, c=0]) => {
      let s = format.num(b);

      if(c){
        s = `${s} B`;
        const s1 = format.bytes(b);
        if(s1 !== s) s = `${s1} (${s})`;
      }

      return `${a}: ${s}`;
    }).join('\n');

    await tick();
  }

  let cur_size = 0;

  const lab_status = ce_lab();
  const pb = O.ce(div, 'progress');
  pb.max = max(total_size, 1);

  let s_prev = null;
  
  const update_status = async () => {
    let s = percent(total_size, cur_size);
    if(s === s_prev) return;

    s_prev = s;
    lab_status.innerText = `Decrypting... ${s}`;
    pb.value = total_size === 0 ? 1 : cur_size;

    return tick(1);
  };

  await update_status();

  const queue = [['', dest_pth, salt_size + 8 * 4]];

  while(queue.length !== 0){
    const [dir, dir1, dir_addr] = queue.shift();

    const files_num = index_file.read_uint(dir_addr);
    const files_addr = dir_addr + 8;
    const dirs_num_addr = dir_addr + (files_num + 1) * 8;
    const dirs_num = index_file.read_uint(dirs_num_addr);
    const dirs_addr = dirs_num_addr + 8;

    for(let i = 0; i !== files_num; i++){
      const addr = index_file.read_uint(files_addr + i * 8);
      const name = index_file.read_str(addr);
      
      const pth = pth_join(dir, `\x5F${i}`);
      const pth_act = pth_join(src_pth, pth);
      const pth1 = pth_join(dir1, name);
      
      // Decrypt file
      
      const size = dec_file(fse, pth, pth_act, pth1);
      
      cur_size += size;
      await update_status();
    }
    
    for(let i = 0; i !== dirs_num; i++){
      const addr = index_file.read_uint(dirs_addr + i * 8);
      const name = index_file.read_str(addr);
      const addr1 = index_file.read_uint(index_file.addr1);

      const pth = pth_join(dir, `${i}`);
      const pth1 = pth_join(dir1, name);

      fs.mkdirSync(pth1);

      queue.push([pth, pth1, addr1]);
    }
  }

  index_file.close();

  assert(cur_size === total_size);
  ce_lab().innerText = 'Done';
};

const decrypt_file = async (src_pth, dest_pth, pw) => {
  const parts = src_pth.split(/[\/\\]/);

  src_pth = parts.shift();
  if(src_pth === '') src_pth = '/';

  let found = 0;

  while(parts.length !== 0){
    const name = parts[0];
    const stat = get_stat(src_pth);

    assert(stat !== null);
    assert(stat.isDirectory());

    const pth = pth_join(src_pth, index_file_name);
    
    if(fs.existsSync(pth)){
      found = 1;
      break;
    }
    
    src_pth = pth_join(src_pth, name);
    parts.shift();
  }

  assert(found);
  assert(parts.length !== 0);
  
  const index_file_pth = index_file_name;
  const index_file_pth_act = pth_join(src_pth, index_file_name);

  const fd = fs.openSync(index_file_pth_act, 'r');
  const salt = Buffer.alloc(salt_size);
  const salt_size1 = fs.readSync(fd, salt, 0, salt_size, 0);

  fs.closeSync(fd);
  assert(salt_size1 === salt_size);

  const fse = new FSE(pw, salt);

  const index_file = fse.open(index_file_pth_act,
    index_file_pth, 'r');
  
  const csum = index_file.read_uint(salt_size);

  if(csum !== 0){
    O.body.innerText = 'Wrong password';
    return;
  }

  let dir = '';
  let dir_addr = salt_size + 8 * 4;

  main_loop: while(1){
    const name = parts.shift();
    const is_file = parts.length === 0;
    const is_dir = !is_file;
    
    const files_num = index_file.read_uint(dir_addr);
    const files_addr = dir_addr + 8;
    const dirs_num_addr = dir_addr + (files_num + 1) * 8;
    const dirs_num = index_file.read_uint(dirs_num_addr);
    const dirs_addr = dirs_num_addr + 8;

    if(is_dir){
      for(let i = 0; i !== dirs_num; i++){
        const addr = index_file.read_uint(dirs_addr + i * 8);
        const name1 = index_file.read_str(addr);
        if(name1 !== name) continue;

        dir = pth_join(dir, `${i}`);
        dir_addr = index_file.read_uint(index_file.addr1);

        continue main_loop;
      }

      assert.fail();
    }

    for(let i = 0; i !== files_num; i++){
      const addr = index_file.read_uint(files_addr + i * 8);
      const name1 = index_file.read_str(addr);
      if(name1 !== name) continue;
      
      const pth = pth_join(dir, `\x5F${i}`);
      const pth_act = pth_join(src_pth, pth);
      
      // Decrypt file

      const size = dec_file(fse, pth, pth_act, dest_pth);
      
      ce_lab().innerText = 'Done';
      
      return;
    }

    assert.fail();
  }
};

const browse = async (src_pth, pw) => {
  const index_file_pth = index_file_name;
  const index_file_pth_act = pth_join(src_pth, index_file_name);

  const fd = fs.openSync(index_file_pth_act, 'r');
  const salt = Buffer.alloc(salt_size);
  const salt_size1 = fs.readSync(fd, salt, 0, salt_size, 0);

  fs.closeSync(fd);
  assert(salt_size1 === salt_size);

  const fse = new FSE(pw, salt);

  const index_file = fse.open(index_file_pth_act,
    index_file_pth, 'r');
  
  const csum = index_file.read_uint(salt_size);

  if(csum !== 0){
    O.body.innerText = 'Wrong password';
    return;
  }

  const open_dir = (info=null, dir0, dir, dir_addr) => {
    init_div();

    const e = O.ceDiv(div, 'block-wrap');
    e.innerText = pth_join(src_pth, dir0);
    O.ceBr(div);

    const info1 = {
      prev: info,
      dir0,
      dir,
      dir_addr,
    };

    const files_num = index_file.read_uint(dir_addr);
    const files_addr = dir_addr + 8;
    const dirs_num_addr = dir_addr + (files_num + 1) * 8;
    const dirs_num = index_file.read_uint(dirs_num_addr);
    const dirs_addr = dirs_num_addr + 8;

    const mk_dir = (info, lab, dir0, dir, dir_addr) => {
      const e = O.ceDiv(div, 'block-wrap');
      const link = O.ceDiv(e, 'fs-entry');
      link.innerText = lab;

      O.ael(link, 'click', evt => {
        open_dir(info, dir0, dir, dir_addr);
      });
    };

    const mk_file = (info, lab) => {
      const e = O.ceDiv(div, 'block-wrap');
      const link = O.ceDiv(e, 'fs-entry');
      link.innerText = lab;

      O.ael(link, 'click', evt => {
        // // Decrypt file
        //
        // const size = dec_file(fse, pth, pth_act, dest_pth);
        //
        // ce_lab().innerText = 'Done';
        //
        // return;
      });
    };

    if(info !== null)
      mk_dir(info.prev, '..', info.dir0, info.dir, info.dir_addr);

    for(let i = 0; i !== dirs_num; i++){
      const addr = index_file.read_uint(dirs_addr + i * 8);
      const name1 = index_file.read_str(addr);

      const dir0_new = pth_join(dir0, name1);
      const dir1 = pth_join(dir, `${i}`);
      const dir_addr1 = index_file.read_uint(index_file.addr1);

      mk_dir(info1, `${name1}/`, dir0_new, dir1, dir_addr1);
    }

    for(let i = 0; i !== files_num; i++){
      const addr = index_file.read_uint(files_addr + i * 8);
      const name1 = index_file.read_str(addr);
      
      const pth = pth_join(dir, `\x5F${i}`);
      const pth_act = pth_join(src_pth, pth);

      mk_file(info1, name1);
    }
  };

  let dir_addr = salt_size + 8 * 4;

  open_dir(null, '', '', dir_addr);
};

const dec_file = (fse, pth, pth_act, pth1) => {
  const stat = get_stat(pth_act);
  const {size} = stat;
  const file = fse.open(pth_act, pth, 'r');
  const fd = fs.openSync(pth1, 'w');

  // file.keep_buf = 1;

  for(let i = 0; i !== size;){
    const len = min(chunk_size, size - i);
    
    file.read(chunk_buf, 0, len);
    fs.writeSync(fd, chunk_buf, 0, len);

    i += len;
  }

  file.close();
  fs.closeSync(fd);

  return size;
};

const ce_inp = (lab, is_pw=0) => {
  const inp_div = O.ceDiv(div, 'inp-div');
  const inp_lab = O.ceDiv(inp_div, 'inp-lab');
  inp_lab.innerText = `${lab}:`;

  const inp = O.ce(inp_div, 'input');
  inp.type = flags.show_pw || !is_pw ? 'text' : 'password';

  return inp;
};

const ce_btn = (lab, fn) => {
  const btn = O.ceBtn(div, lab, '', evt => {
    btn.disabled = 1;
    (async () => fn(evt))()//.catch(log);
  });

  return btn;
};

const ce_lab = (lab='') => {
  const e_lab = O.ceDiv(div, 'lab');
  e_lab.innerText = lab;
  return e_lab;
};

const add_style = () => {
  const style = O.ce(O.head, 'style');
  style.innerText = O.rfs(style_file, 1);
};

const get_stat = pth => {
  try{
    return fs.statSync(pth);
  }catch(e){
    return null;
  }
};

const tick = (force=0) => new Promise(res => {
  if(!force) tick_i++;

  if(force || tick_i === tick_n){
    if(!force) tick_i = 0;
    raf(() => {
      res();
    });
    return;
  }

  res();
});

const raf = fn => {
  return requestAnimationFrame(fn);
};

const percent = (n, k) => {
  return O.percent(k, n + 1);
};

const slice = (buf, ...args) => {
  return buf.slice(...args);
};

main();