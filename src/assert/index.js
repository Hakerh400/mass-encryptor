'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');

const assert = (b, msg=null) => {
  if(msg === null)
    msg = 'Assertion error';
  
  if(!b)
    throw new O.AssertionError(msg);
};

const fail = msg => {
  assert(0, msg);
};

module.exports = Object.assign(assert, {
  fail,
});