'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const cwd = __dirname;
const configDir = path.join(cwd, '../../config');
const configFile = path.join(configDir, 'config.txt');

const {platform} = process;

const getConfig = () => {
  let str = O.rfs(configFile, 1);
  
  str = str.
    replace(/[\(\)]/g, '').
    replace(/(?<="),/g, ':');
  
  str = `{${str.trim().slice(1, -1)}}`;
  
  return JSON.parse(str);
};

const config = {...getConfig(),
  'exe': {
    'electron': (
      platform === 'linux' ? '/usr/local/lib/node_modules/electron/dist/electron' :
      platform === 'win32' ? 'C:/Users/User/AppData/Roaming/npm/node_modules/electron/dist/electron.exe' :
      null
    ),
  },
};

module.exports = config;